package com.parse.product.model

import java.util.Date

case class Product(id: Int, title: String, brand: Brand, category: Category, urlList: List[ProductUrl], prices: List[SitePrice])

case class Brand(name: String)

case class Category(name: String)

case class Site(name: String, source: String)

case class ProductUrl(site: Site, url: String)

case class SitePrice(site: Site, prices: Map[Date, BigDecimal])

case class SinglePrice(productId: Int, siteId: Int, date: Date, price: BigDecimal)
