package com.parse.product.model

case class MySqlConnectionInfo(driver: String, url: String, userName: String, password: String)
