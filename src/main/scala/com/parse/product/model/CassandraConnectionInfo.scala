package com.parse.product.model

case class CassandraConnectionInfo(hosts: java.util.List[String], keySpace: String, username: Option[String], password: Option[String])