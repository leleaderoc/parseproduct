package com.parse.product.model

import java.util.Date

import com.parse.product._
import com.websudos.phantom.CassandraTable
import com.websudos.phantom.dsl._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
 * Definition of Cassandra Table
 */
class ProductPrices extends CassandraTable[ConcreteProductPrices, SinglePrice] {

  override val tableName = "ProductPrices"

  object productId extends IntColumn(this) with PartitionKey[Int]
  object siteId extends IntColumn(this) with PrimaryKey[Int] with ClusteringOrder[Int] with Ascending
  object date extends DateColumn(this) with PrimaryKey[Date] with ClusteringOrder[Date] with Ascending
  object price extends BigDecimalColumn(this)

  /**
   * Override
   */
  def fromRow(row: Row): SinglePrice = {
    SinglePrice(
      productId(row),
      siteId(row),
      date(row),
      price(row)
    )
  }
}

/**
 * Abstract child of [[ProductPrices]] for using recording operations
 */
abstract class ConcreteProductPrices extends ProductPrices with RootConnector {

  /**
   * Build query for batch operation
   *
   * @param singlePrice
   * @return insert query
   */
  def storeQuery(singlePrice: SinglePrice) = {
    insert.value(_.productId, singlePrice.productId)
      .value(_.siteId, singlePrice.siteId)
      .value(_.date, singlePrice.date)
      .value(_.price, singlePrice.price)
      .consistencyLevel_=(ConsistencyLevel.ALL)
  }

  /**
   * Create [[ProductPrices]] table on Cassandra
   */
  def createTable = Await.ready(this.create.ifNotExists().future(), 5.seconds)

  /**
   * Execute batch operation for prices of a single product
   *
   * @param prices
   * @param productId
   * @param siteId
   * @return Future of [[ResultSet]]
   */
  def storeBatch(prices: List[SitePrice], productId: Int, siteId: Int): Future[ResultSet] = {

    val resultBatch = Batch.unlogged |> (
      batch =>
        prices match {
            // There are only prices from one site, no need to check rest of the list
          case sitePrice :: xs =>
            sitePrice.prices.foldLeft(batch) {
              case (batchAcc, (date, price)) =>
                batchAcc.add(
                  storeQuery(SinglePrice(productId, siteId, date, price))
                )
            }
          case _ =>
            batch
        }
      )

    resultBatch.future()
  }
}
