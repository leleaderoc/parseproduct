package com.parse.product.helper

import java.util.Date

import com.parse.product._
import com.parse.product.model._

import java.io.FileInputStream
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import scala.util.{Failure, Success, Try}

object XmlParser {

  private val schemaLang = "http://www.w3.org/2001/XMLSchema"
  private val factory = SchemaFactory.newInstance(schemaLang)
  private val xsdStream = getClass.getResourceAsStream("/products.xsd")
  private val schema = factory.newSchema(new StreamSource(xsdStream))
  private val validator = schema.newValidator()

  private def validate(xmlPath: String): Unit = {
    val xmlStream = new FileInputStream(xmlPath)
    validator.validate(new StreamSource(xmlStream))
    xmlStream.close()
  }

  /** Validates an XML with resources/products.xsd and parses it
    *
    *  @param xmlPath  Directory of the xml
    *  @return Either wrapper of the xml result or an error message
    */
  def parseXml(xmlPath: String): Either[String, xml.Elem] =
    Try (validate(xmlPath)) match {
      case Success(_) => Right(xml.XML.loadFile(xmlPath))
      case Failure(ex) => Left(ex.getMessage)
    }

  /** Parse single product from a node
    *
    *  @param productNode  xml node to be parsed
    *  @return [[Product]]
    */
  def parseProduct(productNode: scala.xml.NodeSeq, site: Site): Product = {

    def parseIfNonEmpty(text: String) = text match {
      case s if s.trim.nonEmpty => s.split(",")
      case _ => Array.empty[String]
    }

    val id = (productNode \ idLabel).text.toInt
    val title = (productNode \ titleLabel).text
    val brand = Brand((productNode \ brandLabel).text)
    val category = Category((productNode \ categoryLabel).text)
    val url = ProductUrl(site, (productNode \ urlLabel).text)

    val prices = parseIfNonEmpty((productNode \ pricesLabel).text)
    val dates = parseIfNonEmpty((productNode \ datesLabel).text)

    val priceMap = (dates, prices).zipped.foldLeft(Map.empty[Date, BigDecimal]) {
      case (mapAcc, (d, p)) => mapAcc + (new Date(d.toLong) -> BigDecimal(p))
    }

    val sitePrice = SitePrice(site, priceMap)

    Product(id, title, brand, category, List(url), List(sitePrice))
  }

  /** Parse all products from a source xml
    *
    *  @param site  information of source site
    *  @return Sequence of either a product or error message
    */
  def parseProducts(site: Site): Seq[Either[String, Product]] = {
    parseXml(site.source) match {

      case Left(msg) =>
        Seq(Left(s"Cannot parse XML: $msg"))

      case Right(xml) =>
        (xml \ rowLabel) map { p =>

          Try(parseProduct(p, site)) match {
            case Success(product) => Right(product)
            case Failure(ex) => Left(
              s"""
                 |Error at parsing node:
                 |$p
                 |Error: $ex
               """.stripMargin
            )
          }
        }
    }
  }
}