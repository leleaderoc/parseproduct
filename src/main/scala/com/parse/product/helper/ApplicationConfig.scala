package com.parse.product.helper

import com.parse.product.model.{CassandraConnectionInfo, MySqlConnectionInfo, Site}
import com.typesafe.config.ConfigFactory
import scala.collection.JavaConversions._

/**
 * Object for handling config parsing
 */

object ApplicationConfig {

  private val config = ConfigFactory.load()


  /**
   * Get xml resources
   *
   * @return xml resources in [[Site]] format
   */
  def getXmlSources: List[Site] = config.getConfigList("application.source-list").toList map (
    c => Site(c.getString("site"), c.getString("path"))
    )

  /**
   * Get MySql connection info
   *
   * @return [[MySqlConnectionInfo]]
   */
  def getMySqlConnectionInfo: MySqlConnectionInfo = {
    val dbInfo = config.getConfig("db.mysql")
    MySqlConnectionInfo(dbInfo.getString("driver"), dbInfo.getString("url"),
      dbInfo.getString("user"), dbInfo.getString("password"))
  }

  /**
   * Get Cassandra connection info
   *
   * @return [[CassandraConnectionInfo]]
   */
  def getCassandraConnectionInfo: CassandraConnectionInfo = {
    val dbInfo = config.getConfig("db.cassandra")

    def getOptionString(key: String) = {
      if (dbInfo.hasPath(key))
        Some(dbInfo.getString(key))
      else
        None
    }

    CassandraConnectionInfo(dbInfo.getStringList("hosts"), dbInfo.getString("key-space"),
      getOptionString("username"), getOptionString("password"))
  }

}