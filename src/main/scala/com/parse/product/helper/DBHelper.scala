package com.parse.product.helper

import java.sql.Connection

import com.parse.product.model._
import com.parse.product.provider.{CassandraConnector, MySqlConnector}

object DBHelper extends ConcreteProductPrices with MySqlConnector with CassandraConnector {

  createTable

  /**
   * Insert collection of products at MySql and Cassandra both
   *
   * @param products: Collection of products
   * @param siteId: Auto-generated site id by MySql
   * @return
   */
  def insertProducts(products: Seq[Either[String, Product]], siteId: Int) = {

    try {
      DB.withConnection {
        implicit connection: Connection =>

          val statement = connection.prepareStatement("call insert_product( ?, ?, ?, ?, ?, ?)")

          products.foreach {
            case Right(product) =>
              // Add Parameters for MySql
              statement.setInt(1, product.id)
              statement.setString(2, product.title)
              statement.setString(3, product.brand.name)
              statement.setString(4, product.category.name)

              product.urlList match {
                case ProductUrl(site, url) :: xs =>
                  statement.setInt(5, siteId)
                  statement.setString(6, url)
                case Nil =>
                  statement.setString(5, null)
                  statement.setString(6, null)
              }

              // Add batch for MySql
              statement.addBatch()

              // Add prices to Cassandra
              storeBatch(product.prices, product.id, siteId)
          }

          // Execute batch for MySql
          statement.executeBatch()
      }
    } catch {
      case ex: Exception =>
        println(s"Error while recording products: ${ex.getMessage}")
    }
  }

  /**
   * Insert site to MySql and return auto-generated site id
   *
   * @param site
   * @return [[Int]]
   */
  def insertSite(site: Site): Int = {
    try {
      DB.withConnection {
        implicit connection: Connection =>

          val statement = connection.prepareStatement("call insert_site( ? )")

          statement.setString(1, site.name)

          val rs = statement.executeQuery()
          rs.next()

          // Return auto-increment id
          rs.getInt(1)
      }
    }
  }

  /**
   * Close cassandra session and cluster
   */
  def closeCassandra() = disconnect
}