package com.parse.product.provider

import java.sql.{DriverManager, Connection}

import com.parse.product.helper.ApplicationConfig

trait MySqlConnector {

  private val connectionInfo = ApplicationConfig.getMySqlConnectionInfo

  object DB {

    /**
     * Execute a MySql transaction by wrapping with try-finally block
     *
     * @param block
     * @return Generic Type
     */
    def withConnection[A](block: Connection => A): A = {

      Class.forName(connectionInfo.driver)
      val connection = DriverManager.getConnection(connectionInfo.url, connectionInfo.userName, connectionInfo.password)

      try {
        block(connection)
      } finally {
        connection.close()
      }
    }
  }
}
