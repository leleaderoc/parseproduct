package com.parse.product.provider

import java.net.InetAddress

import com.datastax.driver.core.Cluster
import com.parse.product.helper.ApplicationConfig
import com.parse.product._
import com.websudos.phantom.connectors.{KeySpace, SessionProvider}
import com.websudos.phantom.dsl._

import scala.collection.JavaConversions._

trait CassandraConnector extends SessionProvider {

  implicit val space: KeySpace = Connector.keyspace

  val cluster = Connector.cluster

  override implicit lazy val session: Session = Connector.session

  def disconnect = {
    session.close()
    cluster.close()
  }
}

/**
 * Main connector object to Cassandra
 */
private object Connector {

  // Connection Info
  val connectionInfo = ApplicationConfig.getCassandraConnectionInfo

  val inets = connectionInfo.hosts.map(InetAddress.getByName)

  val keyspace: KeySpace = KeySpace(connectionInfo.keySpace)

  val cluster =
    (Cluster.builder()
      .addContactPoints(inets) |> (
      builder =>
        (connectionInfo.username, connectionInfo.password) match {
          case (Some(username), Some(password)) =>
            builder.withCredentials(username, password)
          case _ => builder
        }
      )).build()

  val session: Session = cluster.connect(keyspace.name)

}