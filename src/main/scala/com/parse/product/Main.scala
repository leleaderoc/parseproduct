package com.parse.product

import com.parse.product.helper.{DBHelper, ApplicationConfig, XmlParser}

object Main extends App {

  // For each xml resources
  ApplicationConfig.getXmlSources foreach {
    site =>
      // Get products and errors from XmlParser
      val (products, errors) = XmlParser.parseProducts(site) partition(_.isRight)
      errors.foreach(e => println(e.left.get))

      // Insert Site and get generated site id
      val siteId = DBHelper.insertSite(site)

      // Insert operations of products
      DBHelper.insertProducts(products, siteId)
  }

  // Close connection of Cassandra in safe mode
  DBHelper.closeCassandra()

}