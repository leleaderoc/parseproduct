package com.parse

package object product {

  val idLabel = "id"
  val titleLabel = "title"
  val brandLabel = "brand"
  val categoryLabel = "category"
  val urlLabel = "url"
  val pricesLabel = "prices"
  val datesLabel = "dates"
  val rowLabel = "row"

  // Implicit Forward pipe operation
  implicit class PipedObject[A](value: A) {
    def |>[B](f: A => B): B = f(value)
  }

}
