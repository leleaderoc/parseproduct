name := "parse-product"

version := "1.0"

scalaVersion := "2.10.5"

val phantomVersion = "1.18.1"

resolvers ++= Seq(
  "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/",
  "Websudos releases"                at "https://dl.bintray.com/websudos/oss-releases/"
)

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.0",
  "mysql" % "mysql-connector-java" % "5.1.28",
  "com.websudos"  %% "phantom-dsl" % phantomVersion,
  "com.websudos"  %% "phantom-testkit" % phantomVersion
)