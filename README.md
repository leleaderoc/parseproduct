# README #

Product Parser

### What is this repository for? ###

* This project is created for the challenge organised by Cimri

### How do I get set up? ###

* Create a MySql schema regarding the file /evolutions/MySql.sql
* Create a key space at Cassandra same with the one at db.cassandra.key-space at application.conf
* Scala version: 2.10.5
* Xml resources, Cassandra and MySql connection information are defined at src/main/resources/application.conf

### Who do I talk to? ###

* Evaluator of the challenge